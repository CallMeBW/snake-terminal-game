FROM node:16.13.1
RUN npm install -g npm@8.3.0
WORKDIR /app
COPY . /app
RUN npm ci
RUN npm run build
ENTRYPOINT ["npm" , "start"]