# Snake in your terminal!
```
oooooooo8 oooo   oooo     o      oooo   oooo ooooooooooo 
888         8888o  88     888      888  o88    888    88  
888oooooo  88 888o88    8  88     888888      888ooo8    
        888 88   8888   8oooo88    888  88o    888    oo  
o88oooo888 o88o    88 o88o  o888o o888o o888o o888ooo8888 
```
![](images/screen.png)

## How to play
`
./start_in_docker.sh
`

Control the game with your arrow keys. Don't let the snake collide with itself. 

Alternatively, using Node.js v.16, you can start the game like so:

````
npm install
npm build
npm start
````

## Features

### Custom Game Size
You can start the game with width and height arguments to specify the size of the game, e.g. `./start_in_docker.sh 20 2` or `npm start -- 30 10`.

### Input Buffering
You can change the direction of the snake for the current and the subsequent step by pressing two different arrow keys quickly after each other. This allows precise steering in narrow areas.

### Walk through walls
Needless to say, the snake can walk through walls and will appear at the opposite side again.
