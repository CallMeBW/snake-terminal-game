import SnakeGame from './snakeGame'
import Snake from "./snake";

class Terminal {

  static CLEAR = '\x1bc';
  static HEAD = '🟪';
  static HEAD_HIGHLIGHT = '⬜';
  static BODY = '🟫';
  static GROUND = '⬛';
  static FOOD = '🟩';
  static SPLASH = `
    oooooooo8 oooo   oooo     o      oooo   oooo ooooooooooo 
    888         8888o  88     888      888  o88    888    88  
    888oooooo  88 888o88    8  88     888888      888ooo8    
            888 88   8888   8oooo88    888  88o    888    oo  
    o88oooo888 o88o    88 o88o  o888o o888o o888o o888ooo8888 
  `

  drawSplashScreen = () => {
    console.log(`${Terminal.CLEAR}${Terminal.SPLASH}`);
  }

  drawGame = (game: SnakeGame) => {
    const charRows: string[][] = [];
    for (let i = 0; i < game.size[1]; i++) {
      charRows.push(Array(game.size[0]).fill(Terminal.GROUND));
    }
    if (game.foodLocation !== undefined) {
      charRows[game.foodLocation[1]][game.foodLocation[0]] = Terminal.FOOD;
    }
    let headChar = Terminal.HEAD;
    if(game.stepsUntilSpeedUp == 1){
      headChar = Terminal.HEAD_HIGHLIGHT;
    }
    for (let i = 0; i < game.snake.body.length; i++) {
      const bodyPart = game.snake.body[game.snake.body.length-1-i]
      charRows[bodyPart[1]][bodyPart[0]] = i == 0 ? headChar : Terminal.BODY;
    }
    let output = 
      Terminal.CLEAR
      + charRows
          .map(charRow => charRow.join('') + '\n')
          .join('')
      + `Score: ${game.score()}`;
    console.log(output);
  }
}

export default Terminal;