import { Direction, Location } from "./types";
import { arraysEqual } from "./utils";

class Snake {
  static INITIAL_LENGTH = 5;
  length: number;
  body: Location[] = [[0, 0]];
  direction: Direction = [1, 0];

  constructor() {
    this.length = Snake.INITIAL_LENGTH;
  }

  getHeadLocation() {
    return this.body[this.body.length-1];
  }

  move = () => {
    const head: Location = [...this.getHeadLocation()];
    head[0] += this.direction[0];
    head[1] += this.direction[1];
    this.body.push(head);
  }

  didCollide = () => {
    const bodyWithoutHead = this.body.slice(0,-1);
    const headLoc = this.getHeadLocation(); 
    return bodyWithoutHead.findIndex(loc => arraysEqual(loc, headLoc)) >= 0;
  }

  retract = (): Location | undefined => {
    if (this.body.length > this.length + 1) {
      return this.body.shift();
    }
  }
}

export default Snake;