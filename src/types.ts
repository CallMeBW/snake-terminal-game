export type Tuple<T> = [T, T];
export type Location = Tuple<number>;
export type Direction = Tuple<-1|0|1>;