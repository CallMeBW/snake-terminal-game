import readLine from 'readline';
import SnakeGame from "./snakeGame";
import { Tuple } from './types';


const registerKeyEventListener = (nameToCallbackMap: { [key: string]: () => void }) => {
  readLine.emitKeypressEvents(process.stdin);
  if (process.stdin.isTTY) {
    process.stdin.setRawMode(true);
  }
  process.stdin.on('keypress', (str, key) => {
    if (key.ctrl && key.name === 'c') {
      process.exit();
    }
    if (key.name in nameToCallbackMap) {
      nameToCallbackMap[key.name]()
    }
  });
}

const main = () => {
  let size: Tuple<number> = undefined;
  if (process.argv.length > 2){
    size = [parseInt(process.argv[2]), parseInt(process.argv[3])];
  }
  const snakeGame = new SnakeGame(size);
  registerKeyEventListener({
    up: () => snakeGame.onSetDirectionDelta([0, -1]),
    down: () => snakeGame.onSetDirectionDelta([0, 1]),
    left: () => snakeGame.onSetDirectionDelta([-1, 0]),
    right: () => snakeGame.onSetDirectionDelta([1, 0]),
  });
}

main();