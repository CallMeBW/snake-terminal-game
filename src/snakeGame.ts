import Snake from './snake';
import Terminal from './terminal';
import { Location, Tuple, Direction } from './types';
import { randomInt, arraysEqual } from './utils';

class SnakeGame {
  static SPEED_UP_INTERVAL_MS = 8 * 1000;
  static INITIAL_MOVE_INTERVAL_MS = 700;
  static INITIAL_FOOD_DELAY_MS = 3 * 1000;
  static GAME_SIZE: Tuple<number> = [20, 10];
  static SPEED_UP_FACTOR = 0.9;
  static SPLASH_DURATION = 1000;

  foodLocation: Location;
  size: Tuple<number>;
  terminal: Terminal;
  snake: Snake;
  nextDirections: Direction[] = [];
  stepsUntilSpeedUp: number;
  emptyFields: Location[] = [];
  currentMoveIntervalMS: number;
  moveIntervalTimer: NodeJS.Timer;

  constructor(size?: Tuple<number>) {
    if(size !== undefined && size[0] > 0 && size[1] > 0){
      this.size = size;
    } else {
      this.size = SnakeGame.GAME_SIZE;
    }
    this.currentMoveIntervalMS = SnakeGame.INITIAL_MOVE_INTERVAL_MS;
    this.snake = new Snake();
    for (let i = 0;i < this.size[0]; i++){
      for (let j = 0;j < this.size[1]; j++){
        const loc: Location = [i, j]
        if(!arraysEqual(loc, this.snake.body[0])){
          this.emptyFields.push(loc);
        }
      } 
    }
    this.terminal = new Terminal();
    this.stepsUntilSpeedUp = Math.ceil(SnakeGame.SPEED_UP_INTERVAL_MS / SnakeGame.INITIAL_MOVE_INTERVAL_MS)
    this.terminal.drawSplashScreen();
    setTimeout(this.start, SnakeGame.SPLASH_DURATION);
  }

  start = () => {
    this.terminal.drawGame(this);
    this.updateMoveInterval();
    setTimeout(this.replaceFood, SnakeGame.INITIAL_FOOD_DELAY_MS);
  }

  updateMoveInterval = () => {
    if (this.moveIntervalTimer) {
      clearInterval(this.moveIntervalTimer);
    }
    this.moveIntervalTimer = setInterval(
      this.onMoveInterval,
      this.currentMoveIntervalMS
    );
  }

  isNewDirectionAllowed = (newDir: Location) => {
    const currentDir = this.snake.direction;
    const delta = [currentDir[0] - newDir[0], currentDir[1] - newDir[1]].map(Math.abs);
    return delta[0] < 2 && delta[1] < 2 && !arraysEqual(newDir, currentDir);
  }

  onSetDirectionDelta = (newDirection: Direction) => {
    if (this.isNewDirectionAllowed(newDirection)) {
      this.nextDirections = [newDirection];
    } else if (this.nextDirections.length == 1){
      this.nextDirections.push(newDirection);
    }
  }

  warp = () => {
    const pos = this.snake.getHeadLocation();
    const newPos: Location = [
      pos[0] == this.size[0] ? 0 : pos[0] == -1 ? this.size[0] - 1 : pos[0],
      pos[1] == this.size[1] ? 0 : pos[1] == -1 ? this.size[1] - 1 : pos[1]
    ];
    this.snake.body[this.snake.body.length - 1] = newPos;
  }

  replaceFood = () => {
    if(this.emptyFields.length === 0){
      this.foodLocation = undefined;
    } else {
      this.foodLocation = this.emptyFields[randomInt(0, this.emptyFields.length - 1)]
    }
  }

  score = () => {
    return this.snake.length - Snake.INITIAL_LENGTH;
  }

  onMoveInterval = () => {
    const [dir1, dir2] = this.nextDirections;
    if (dir1 !== undefined){
      this.snake.direction = dir1;
    }
    if (dir2 !== undefined && this.isNewDirectionAllowed(dir2)){
      this.nextDirections = [dir2]
    }
    this.snake.move();
    this.warp();
    const emptyField = this.snake.retract();
    if(emptyField){
      this.emptyFields.push(emptyField)
    }
    const head = this.snake.getHeadLocation();
    this.emptyFields = this.emptyFields.filter(field => !arraysEqual(field, head))
    if (arraysEqual(this.foodLocation, head)) {
      this.snake.length++;
      this.replaceFood();
    }
    if (this.snake.didCollide()) {
      console.log("\n\nGame Over!");
      process.exit(0);
    }
    this.terminal.drawGame(this);
    this.stepsUntilSpeedUp--;
    if (this.stepsUntilSpeedUp <= 0){
      // we update the speed interval after n steps, rather than at a fixed interval to prevent
      // that the current interval is cut off off-rhythm.
      this.currentMoveIntervalMS *= SnakeGame.SPEED_UP_FACTOR;
      this.stepsUntilSpeedUp = Math.ceil(SnakeGame.SPEED_UP_INTERVAL_MS / this.currentMoveIntervalMS)
      this.updateMoveInterval();
    }
  }
}
export default SnakeGame;